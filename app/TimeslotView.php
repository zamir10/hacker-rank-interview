<?php

class TimeslotView
{
    /**
     * @var Timeslot
     */
    private $timeslot;

    /**
     * @param Timeslot $timeslot
     */
    public function __construct(Timeslot $timeslot)
    {
        $this->timeslot = $timeslot;
    }

    /**
     * @return int
     */
    public function getDurationInMinutes()
    {
        /**
         * @TODO: Implementation
         */
        $d = $this->timeslot->getEndsAt()->diff($this->timeslot->getStartsAt());
        return $d->i + $d->h * 60;
    }

    /**
     * @param int $length
     * @return string
     */
    public function getDescriptionExcerpt(int $length = 10)
    {
        /**
         * @TODO: Implementation
         */
        return mb_substr($this->timeslot->getDescription(), 0, $length);
    }
}
