<?php

/**
 * Class Schedule
 */
class Schedule implements Iterator, Countable
{
    /**
     * @var array
     */
    private $timeslots;

    /**
     * @var int
     */
    private $index = 0;

    /**
     *
     */
    public function __construct()
    {
        $this->timeslots = array();
    }

    /**
     * @param Timeslot $timeslot
     * @return $this
     */
    public function addTimeslot(Timeslot $timeslot)
    {
        if (!$this->overlaps($timeslot)) {
            $this->timeslots[] = $timeslot;
        }

        $this->sortByStartTime();
    }

    /**
     * Sort slots by starting time
     */
    private function sortByStartTime()
    {
        usort($this->timeslots, function (Timeslot $d1, Timeslot $d2) {
            /**
             * @TODO: Implementation
             */

            if ($d1->getStartsAt() == $d2->getStartsAt()) {
                return 0;
            }

            return $d1->getStartsAt() < $d2->getStartsAt() ? -1 : 1;
        });
    }

    /**
     * @param Timeslot $timeslot
     * @return bool
     */
    public function overlaps(Timeslot $timeslot)
    {
        foreach ($this->timeslots as $existingSlot) {
            if ($timeslot->overlaps($existingSlot)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public function count()
    {
        /**
         * @TODO: Implementation
         */

        return count($this->timeslots);
    }

    /**
     * @return void
     */
    function rewind()
    {
        /**
         * @TODO: Implementation
         */
        $this->index = 0;
    }

    /**
     * @return mixed
     */
    function current()
    {
        /**
         * @TODO: Implementation
         */

        return $this->timeslots[$this->index];
    }

    /**
     * @return mixed
     */
    function key()
    {
        /**
         * @TODO: Implementation
         */
        return $this->index;
    }

    /**
     * @return void
     */
    function next()
    {
        /**
         * @TODO: Implementation
         */
        $this->index++;
    }

    /**
     * @return bool
     */
    function valid()
    {
        /**
         * @TODO: Implementation
         */

        return !is_null($this->timeslots[$this->index]);
    }
}
